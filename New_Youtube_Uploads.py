import json
import time
import urllib.request

from selenium import webdriver


def look_for_new_videos():
    api_key = "AIzaSyCCbWj5FL7WBIViXFcmLjKKlOqqDFSDEwc"  # add api key from console.developers.google.com
    channel_id = "UCQN2DsjnYH60SFBIA6IkNwg"  # add channel id

    video_url = 'https://www.youtube.com/watch?v='
    search_url = 'https://www.googleapis.com/youtube/v3/search?'

    url = search_url + 'key={}&channelId={}&part=snippet,id&order=date&maxResults=1'.format(api_key, channel_id)
    print("Sending request...")
    request = urllib.request.urlopen(url)
    response = json.load(request)

    videoId = response['items'][0]['id']['videoId']
    print("---------------------Current Id:" + videoId + "---------------------")
    video_exists = False
    with open('videoId.json', 'r') as json_file:
        data = json.load(json_file)
        if data['videoId'] != videoId:
            print("---------------------Opening the latest video...---------------------")
            driver = webdriver.Firefox()  # need to add gecko driver to run on firefox
            driver.get(video_url + videoId)
            video_exists = True
        print("---------------------Sadly, No new uploads from STÖK---------------------")
    if video_exists:
        with open('videoId.json', 'w') as json_file:
            print("---------------------Writing current id to json file...---------------------")
            data = {'videoId': videoId}
            json.dump(data, json_file)


try:
    while True:
        look_for_new_videos()
        print("---------------------Next check will be after 24 hour.---------------------")
        time.sleep(1*24*60*60)
except KeyboardInterrupt:
    print('---------------------Exiting...---------------------')
