import os
import time

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer


class FileHandler(FileSystemEventHandler):
    def on_modified(self, event):
        for filename in os.listdir(track_folder):
            i = 1
            if filename != 'Predator':
                new_name = filename
                file_exists = os.path.isfile(destination_folder + '/' + new_name)
                while file_exists:
                    i += 1
                    new_name = os.path.splitext(track_folder + '/' + new_name)[0] + str(i) + \
                               os.path.splitext(track_folder + '/' + filename)[1]
                    new_name = new_name.split("/")[4]
                    file_exists = os.path.isfile(destination_folder + "/" + new_name)

                src = destination_folder + "/" + filename
                new_name = track_folder + "/" + new_name
                os.rename(src, new_name)


track_folder = '/Users/yunish/Desktop'
destination_folder = '/Users/yunish/Desktop/Predator'
event_handler = FileHandler()
observer = Observer()
observer.schedule(event_handler, track_folder, recursive=True)
observer.start()

try:
    while True:
        time.sleep(10)

except KeyboardInterrupt:
    observer.stop()
observer.join()
